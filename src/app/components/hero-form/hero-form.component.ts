import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';

import { Hero } from '../../models/hero';
import { Power } from '../../models/power';

@Component({
  selector: 'app-hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.css']
})
export class HeroFormComponent implements OnInit {
  // powers = ['Really Smart', 'Super Flexible',
  //           'Super Hot', 'Weather Changer'];
            
  powerArr: Array<Power> = [];
  model: Hero;  

  submitted = false;
  constructor() {
    this.powerArr.push(
      { id: 1, name: 'Really Smart' },
      { id: 2, name: 'Super Flexible' },
      { id: 3, name: 'Super Hot' },
      { id: 4, name: 'Weather Changer' });

    this.model = new Hero(18, 'Dr IQ', 'varigela@ivycomptech.com', this.powerArr[1].id, 'Chuck Overstreet');
    

  }

  ngOnInit() {
    
  }

  onSubmit(heroForm: NgForm) {
	  debugger;
    if(heroForm.form.valid){
      console.log('valid form');
      console.log(heroForm.form.value);
      this.submitted = true;
    } else{
      console.log('invalid form');
    }   
  }

  onPowerChanged(power: number) {
    console.log('in onpowerchanged')
  }

  onTemperatureRequired(power: number) {
    console.log('in');
    return power == 4;
  }

  onPhoneRequired(phone: string) {
    if(phone != null && phone.length > 0){
      return true;
    } else{
      return false;
    }
  }

  get diagnostic() {
    return JSON.stringify(this.model);
  }

}
