import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Hero } from '../../models/hero';
import { Power } from '../../models/power';

@Component({
    selector: 'app-hero-form2',
    templateUrl: './hero-form2.component.html',
    styleUrls: ['./hero-form2.component.css']
})
export class HeroForm2Component implements OnInit, AfterViewInit {
    powerArr: Array<Power> = [];
    model: Hero;
    @ViewChild('heroForm') heroNgForm: NgForm;
    @ViewChild('tempRef') tempRef: ElementRef;

    submitted = false;
    constructor() {
        this.powerArr.push(
            { id: 1, name: 'Really Smart' },
            { id: 2, name: 'Super Flexible' },
            { id: 3, name: 'Super Hot' },
            { id: 4, name: 'Weather Changer' });

        this.model = new Hero(18, 'Dr IQ', 'varigela@ivycomptech.com', this.powerArr[1].id, 'Chuck Overstreet', 10, '9876543210');
    }

    ngAfterViewInit() {

    }

    ngOnInit() {

    }

    onSubmit(heroForm: NgForm) {
        if (heroForm.form.valid) {
            console.log('valid form');
            console.log(heroForm.form.value);
            this.submitted = true;
        } else {
            console.log('invalid form');
        }
    }

    onPowerChanged(power: number) {
        console.log('in onpowerchanged')
    }

    onTemperatureRequired(power: number) {
        console.log('in');
        return power == 4;
    }

    onPhoneRequired(phone: string) {
        if (phone != null && phone.length > 0) {
            return true;
        } else {
            return false;
        }
    }
}
