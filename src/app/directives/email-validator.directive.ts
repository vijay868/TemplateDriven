import { Directive } from '@angular/core';
import { FormControl, NG_VALIDATORS } from '@angular/forms';

function emailDomainValidator(control: FormControl){
  let email = control.value;
  if(email && email.indexOf('@') != -1){
    let [_, domain] = email.split('@');
    if(domain != 'ivycomptech.com'){
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
    return null;
  }
}

@Directive({
  selector: '[emailDomain][ngModel]',
  providers: [{
      provide: NG_VALIDATORS,
      useValue: emailDomainValidator,
      multi: true
    }]
})
export class EmailValidatorDirective {
  constructor() { }
}
