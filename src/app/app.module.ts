import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HeroFormComponent } from './components/hero-form/hero-form.component';
import { HeroForm2Component } from './components/hero-form2/hero-form2.component';
import { EmailValidatorDirective } from './directives/email-validator.directive';


@NgModule({
  declarations: [
    AppComponent,
    HeroFormComponent,
    HeroForm2Component,
    EmailValidatorDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
