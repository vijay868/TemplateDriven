export class Hero {
    // id: number;
    // name: string;
    // power: string;
    // alterEgo?: string;

    constructor(
        public id: number,
        public name: string,
        public email: string,
        public power: number,
        public alterEgo?: string,
        public temperature?: number,
        public phone?: string){

    }
}
